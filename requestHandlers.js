/**
 * Created by Manas on 7/19/2016.
 */


var fs = require("fs"); //Gets file system module and makes it accessible through fs variable


function set(response, postData, id) //Request handler for set
{
    //console.log("ID: "+ id);

    console.log("Request handler 'set' was called.");
    var json;
    if(typeof id == "undefined")
        json = JSON.parse(postData); // Parses post data
    var file = [];
    var count=0;
    var date = new Date();
    //Opens file for reading or appending and creates file if it doesn't exist
    fs.open('./keys.json','a+',function(err,fd) {
        console.log("File opened");
        if(err)
            console.log(err);
        var data = fs.readFileSync('./keys.json'); // Reads the file synchronously
        var dummy= data.toString();
        //Checks if there is data in dummy
        if(dummy){
            file = JSON.parse(data);
            //Checks if there are more tha 1000 entries
            if(file.length>1000)
            {
                response.write("100 Entries exceeded.");
                response.end();
                return;
            }


            for(var i in file){
                //Checks if id is undefined
                if((typeof id) != "undefined") {
                    if (file[i].key.toString() == id.toString()) {
                        file[i].timeStamp = date; //Updates time stamp
                        count++;

                    }
                }
                else if ((file[i].key == json.key) ) {
                    file[i].timeStamp = date;   //Updates time stamp
                    count++;
                }
            }

            if(count == 0)
            {
                json.timeStamp = date; //Updates time stamp
                file.push(json);
            }
        }
        else {
            json.timeStamp = date; //Updates time stamp
            file.push(json);
        }



        fs.writeFile('./keys.json', JSON.stringify(file), function (err) {

        });
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Key and Value Saved");
        response.end();

    });


}

function get(response) //Request handler for get
{
    console.log("Request handler 'upload' was called.");
    var body = '';
    //Opens file for reading or appending and creates file if it doesn't exist
    fs.open('./keys.json','a+',function(err,fd) {
        if(err){
            console.log(err);
        }
        var data = fs.readFileSync('./keys.json'); // Reads the file synchronously
        if(data.toString())
        {
            body = JSON.parse(data);

            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write('Keys\t'+'Time Stamp\n');
            for(var i in body)
                response.write(JSON.stringify(body[i].key)+"\t"+ body[i].timeStamp+"\n");
            response.end();
            //console.log(data.toString());
        }
        else
        {
            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write('No DATA.');
            response.end();
        }
    });


}

exports.set = set;
exports.get = get;