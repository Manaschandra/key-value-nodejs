/**
 * Created by Manas on 7/19/2016.
 */

function route(handle, pathname, request, response, postData,id) {
    console.log("About to route a request for " + pathname);
    //Handles the request handlers
    if (typeof handle[pathname] === 'function' && request.method == 'PUT') {
        return handle[pathname](response, postData, id);
    } else if(typeof handle[pathname] === 'function' && request.method == 'GET'){
        return handle[pathname](response);
    }
    else{
        console.log("No request handler found for " + pathname);
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not found");
        response.end();}
}
exports.route = route;