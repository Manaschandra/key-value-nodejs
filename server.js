/**
 * Created by Manas on 7/19/2016.
 */

var http = require("http"); //Gets http module and makes it accessible through http variable
var qs  = require("querystring");
var url = require("url"); // Gets url module and makes it accessible through url variable

function start(route, handle){
    function onRequest(request, response) //Triggers when a request is made
    {
        var postData = "";
        //if(request.method == 'POST')
        //console.log("METHOD");

        var path1 = url.parse(request.url).pathname; //
        path2= path1.split("/")
        var pathname = "/"+path2[1];
        var id = path2[2];

        console.log("Request for " + pathname + " received.");

        request.setEncoding("utf8");

        request.addListener("data", function(postDataChunk) // Collects data in chunks
        {
            postData += postDataChunk;
            console.log("Received POST data chunk '"+
                postDataChunk + "'.");
        });

        request.addListener("end", function()  //Gets the complete data
        {

            route(handle, pathname, request, response, postData, id); //Call the router to route the requests
        });
    }

    http.createServer(onRequest).listen(8888); //Creates http server and makes it listen on port 8888
    console.log("Server has started.");
}
exports.start = start;
