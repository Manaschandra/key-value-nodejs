/**
 * Created by Manas on 7/19/2016.
 */

var server = require("./server"); //Gets server.js from root directory
var router = require("./router"); //Gets router.js from root directory
var requestHandlers = require("./requestHandlers"); //Gets requestHandler.js from root directory

var handle = {}; //Collection of request handlers
handle["/"] = requestHandlers.set;
handle["/set"] = requestHandlers.set;
handle["/get"] = requestHandlers.get;

server.start(router.route, handle); // Wired router with server