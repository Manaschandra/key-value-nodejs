# Key Value#

Create a server that handles CRU(D) in a basic key-value store and display it.

### Implemetation ###

* Server is built using NodeJs
* No external dependencies were used.
* Data persists on server restart.
* Optimized for no more than 1000 entries.
* Displays the data with human-readable timestamps

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Execution ###

* Open terminal and go to the root of the project. 
* Type node index.js


### Endpoints ###
* PUT, ‘/set’ - update or create a new key.
* GET, ‘/get’ - get the contents of a key

### Note ###
* Use postman to access endpoints
Eg: http://localhost:8888/set, http://localhost:8888/get